﻿module GradeSchool
open System.Collections.Generic

type School = Map<int, string list>

let empty: School = Map.empty

let add (student: string) (grade: int) (school: School): School = 
    let gradeExists = school |> Map.containsKey grade
    match gradeExists with
    | true -> school |> Map.add grade (student :: school.[grade])
    | false -> school |> Map.add grade [student]

let roster (school: School): (int * string list) list = 
    school 
    |> Map.map (fun key value -> value |> List.sort)
    |> Map.toList

let grade (number: int) (school: School): string list = 
    let gradeExists = school |> Map.containsKey number
    match gradeExists with
    | true -> school.[number] |> List.sort
    | false -> List.Empty