﻿module Grains
open System.Numerics

let square (n: int): Result<uint64,string> = 
    let rec sqr (max: int) (acc: BigInteger) (index: int): uint64 = 
        let multiplier: BigInteger =
            match index with
                | index when index < 2-> bigint(1)
                | index -> bigint(2)
        if max < index then
            uint64 (acc)
        else
            sqr max (acc * multiplier) (index + 1)
    match n with
        | n when n < 1 || n > 64 -> Error "Invalid input"
        | _ -> Ok (sqr n (bigint(1)) 0)

let total: Result<uint64,string> = 
    let squares = seq { 1 .. 64 }
    let resultValue (r: Result<uint64,string>): uint64 = 
        match r with
            | Ok res -> res
            | _ -> uint64(0)

    let grainResults =  Seq.map (fun s -> square s) squares
    let results = Seq.map (fun r -> resultValue r) grainResults
    let sum = Seq.sum results
    Ok sum
    
    