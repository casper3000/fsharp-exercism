﻿module SumOfMultiples

let sum (numbers: int list) (upperBound: int): int = 
    let isDivisible number = Seq.exists (fun el -> (number % el) = 0 ) numbers

    let numbersList = [1..(upperBound-1)]
    List.fold (fun total number -> if (isDivisible number) then total + number else total) 0 numbersList