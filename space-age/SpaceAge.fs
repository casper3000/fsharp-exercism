﻿module SpaceAge
open System

// TODO: define the Planet type
type Planet =
    | Earth
    | Mercury
    | Venus
    | Mars
    | Jupiter
    | Saturn
    | Uranus
    | Neptune

let age (planet: Planet) (seconds: int64): float = 
    let orbitalPeriod =
        match planet with
            | Earth -> float 1
            | Mercury -> float 0.2408467
            | Venus -> float 0.61519726
            | Mars -> float 1.8808158
            | Jupiter -> float 11.862615
            | Saturn -> float 29.447498
            | Uranus -> float 84.016846
            | Neptune -> float 164.79132

    (float seconds) / (float 31557600 * orbitalPeriod)