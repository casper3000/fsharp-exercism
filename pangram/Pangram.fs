﻿module Pangram
open System

let isPangram (input: string): bool = 
    let rec pangramTester (input: char list) (usedLetters: char list): bool = 
        printf "%d - " usedLetters.Length
        printfn "%A" usedLetters
        match input with
            | [] when Seq.length (Seq.distinct usedLetters) = 26 -> true
            | [] -> false
            | head :: tail when System.Char.IsLetter(head) -> pangramTester tail (head :: usedLetters)
            | head :: tail -> pangramTester tail usedLetters
    
    match input with
        | "" -> false
        | _ -> 
            let inputChars = input.ToLower() |> Seq.toList
            pangramTester inputChars []


    