﻿module Bob
let response (input: string): string =
    let isSilence (input: string): bool = input.Trim() = ""
    let containsLetter(input: string): bool = input.Trim() |> Seq.exists (fun el -> el |> System.Char.IsLetter)
    let isShouting (input: string): bool = (input.ToUpper() = input) && (containsLetter input)
    let isQuestion (input: string) = input.Trim() |> Seq.last = '?'
    
    match input with
        | input when  input |> isSilence -> "Fine. Be that way!"
        | input when (input |> isShouting) && (input |> isQuestion) -> "Calm down, I know what I'm doing!"
        | input when input |> isShouting -> "Whoa, chill out!"
        | input when input |> isQuestion -> "Sure."
        | _ -> "Whatever."
