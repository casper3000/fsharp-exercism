﻿module Raindrops

let convert (number: int): string =
    let validFactors = [3;5;7]

    let translate (number: int): string =
        match number with
        | 3 -> "Pling"
        | 5 -> "Plang"
        | 7 -> "Plong"
        | _ -> string number

    let rec translateFactors (translation: string) (numbers: int list) =
        match numbers with
        | [] -> if translation = "" then translate number else translation
        | head::tail -> translateFactors (translation + (translate head)) tail

    let factors = List.filter (fun x -> (number % x) = 0 ) validFactors
    
    factors |> translateFactors ""