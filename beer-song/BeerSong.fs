﻿module BeerSong

let recite (startBottles: int) (takeDown: int) = 
    let verse (bottles: int): string list = 
        let bottlesLeft = bottles - 1
        let bottlePuralizer count = 
            match count with
            | 1 -> "bottle"
            | _ -> "bottles"

        let firstLine = 
            let numberOfBottles = 
                match bottles with
                | 0 -> "No more bottles"
                | _ -> sprintf "%i %s" bottles (bottlePuralizer bottles)

            sprintf "%s of beer on the wall, %s of beer." numberOfBottles (numberOfBottles.ToLower())

        let secondLine = 
            match bottlesLeft with
                | -1 -> "Go to the store and buy some more, 99 bottles of beer on the wall."
                | 0 -> "Take it down and pass it around, no more bottles of beer on the wall."
                | _ -> sprintf "Take one down and pass it around, %i %s of beer on the wall." bottlesLeft (bottlePuralizer bottlesLeft)
        
        (firstLine) :: [secondLine]       

    let rec songWriter(bottles: int) (bottlesLeft: int) (verses: string list): string list = 
        match bottlesLeft with
        | bottlesLeft when bottlesLeft = 0 -> verses
        | bottlesLeft when bottlesLeft = 1 -> songWriter (bottles - 1) (bottlesLeft - 1) (verses @ (verse bottles))
        | _ -> songWriter (bottles - 1) (bottlesLeft - 1) (verses @ (verse bottles) @ [""] )

    songWriter startBottles takeDown []