﻿module Hamming

let distance (strand1: string) (strand2: string): int option = 
    if strand1.Length <> strand2.Length then None
    else
        let variations: int = Seq.fold2 (fun distance acid1 acid2 -> if acid1 = acid2 then distance else distance + 1) 0 strand1 strand2
        Some variations