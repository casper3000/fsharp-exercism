﻿module QueenAttack

let create (position: int * int) = 
    let isValidPosition pos = pos >= 0 && pos <= 7

    match position with
    | (x, y) when (x |> isValidPosition) && (y |> isValidPosition) -> true
    | _ -> false

let canAttack (queen1: int * int) (queen2: int * int) =
    match queen1, queen2 with
        | (x1,y1), (x2, y2) when x1 = x2 -> true
        | (x1,y1), (x2, y2) when y1 = y2 -> true
        | (x1,y1), (x2, y2) when ((y2 - y1) / (x2 - x1)) = 1 -> true
        | (x1,y1), (x2, y2) when ((y2 - y1) / (x2 - x1)) = -1 -> true
        | _ -> false